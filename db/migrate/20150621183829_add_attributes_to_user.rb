class AddAttributesToUser < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :profession, :string
    add_column :users, :address, :string
    add_column :users, :website, :string
    add_column :users, :twitter, :string
    add_column :users, :googleplus, :string
    add_column :users, :facebook, :string
    add_column :users, :github, :string
    add_column :users, :linkedin, :string
    add_column :users, :about, :text
  end
end
