class AddInstitutionIdToCertificates < ActiveRecord::Migration
  def change
    add_column :certificates, :institution_id, :integer
  end
end
