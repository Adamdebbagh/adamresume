class CreateCertificates < ActiveRecord::Migration
  def change
    create_table :certificates do |t|
      t.string :institution_title
      t.string :course_title
      t.string :score
      t.string :course_issuer
      t.string :course_instructor
      t.text :description
      t.references :institution, index: true

      t.timestamps null: false
    end
  end
end
