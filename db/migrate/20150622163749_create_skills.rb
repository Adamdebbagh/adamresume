class CreateSkills < ActiveRecord::Migration
  def change
    create_table :skills do |t|
      t.text :intro
      t.string :skill_title
      t.string :skill_level
      t.string :skill_in_percentage

      t.timestamps null: false
    end
  end
end
