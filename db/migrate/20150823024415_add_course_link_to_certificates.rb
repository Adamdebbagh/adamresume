class AddCourseLinkToCertificates < ActiveRecord::Migration
  def change
    add_column :certificates, :course_link, :string
  end
end
