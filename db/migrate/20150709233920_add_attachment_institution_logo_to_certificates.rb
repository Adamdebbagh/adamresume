class AddAttachmentInstitutionLogoToCertificates < ActiveRecord::Migration
  def self.up
    change_table :certificates do |t|
      t.attachment :institution_logo
    end
  end

  def self.down
    remove_attachment :certificates, :institution_logo
  end
end
