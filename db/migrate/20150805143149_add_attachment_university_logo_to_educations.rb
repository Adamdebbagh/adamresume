class AddAttachmentUniversityLogoToEducations < ActiveRecord::Migration
  def self.up
    change_table :educations do |t|
      t.attachment :university_logo
    end
  end

  def self.down
    remove_attachment :educations, :university_logo
  end
end
