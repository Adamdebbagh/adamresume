class AddAttachmentEstablishmentLogoToCourses < ActiveRecord::Migration
  def self.up
    change_table :courses do |t|
      t.attachment :establishment_logo
    end
  end

  def self.down
    remove_attachment :courses, :establishment_logo
  end
end
