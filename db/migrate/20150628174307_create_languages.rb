class CreateLanguages < ActiveRecord::Migration
  def change
    create_table :languages do |t|
      t.string :title
      t.string :level
      t.integer :rate

      t.timestamps null: false
    end
  end
end
