class AddMarkdownToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :markdown, :text
  end
end
