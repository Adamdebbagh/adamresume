class CreateEducations < ActiveRecord::Migration
  def change
    create_table :educations do |t|
      t.string :major
      t.string :university
      t.string :year

      t.timestamps null: false
    end
  end
end
