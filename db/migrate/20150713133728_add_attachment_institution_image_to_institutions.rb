class AddAttachmentInstitutionImageToInstitutions < ActiveRecord::Migration
  def self.up
    change_table :institutions do |t|
      t.attachment :institution_image
    end
  end

  def self.down
    remove_attachment :institutions, :institution_image
  end
end
