class CreateExperiences < ActiveRecord::Migration
  def change
    create_table :experiences do |t|
      t.string :title
      t.string :organization
      t.string :website
      t.string :period
      t.string :description

      t.timestamps null: false
    end
  end
end
