class AddEstablishementInfoToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :establishment, :string
    add_column :courses, :instructor, :string
    add_column :courses, :description, :string
  end
end
