class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.string :caption
      t.text :summary
      t.text :content
      t.string :tag
      t.string :category

      t.timestamps null: false
    end
  end
end
