class Project < ActiveRecord::Base
  is_impressionable

  validates :title, presence:true

  has_attached_file :project_image, :styles => { :large => "900×374", :medium => "300x300>", :thumb => "100x100>" }
  validates_attachment_content_type :project_image, :content_type => /\Aimage\/.*\Z/


end
