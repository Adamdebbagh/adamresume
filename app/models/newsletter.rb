class Newsletter < ActiveRecord::Base
  validates :email, presence:true
  validates :email, length: {minimum: 10}
  validates :email, :format => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  validates :email, :uniqueness => {:message => ' already exist! '}


end
