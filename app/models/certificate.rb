class Certificate < ActiveRecord::Base

  belongs_to :institution
  validates :institution_title,:course_title,:score, presence:true


  has_attached_file :institution_logo, :styles => {:thumb => "100x100>" }
  validates_attachment_content_type :institution_logo, :content_type => /\Aimage\/.*\Z/
end
