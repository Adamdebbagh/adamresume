class Conference < ActiveRecord::Base
  validates :name,:location, presence:true
  validates :link, :format => URI::regexp(%w(http https))
end
