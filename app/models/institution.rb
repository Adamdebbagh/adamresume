class Institution < ActiveRecord::Base
  has_many :certificates

  validates :title, presence:true
  validates :website, :format => URI::regexp(%w(http https))


  has_attached_file :institution_image, :styles => {:medium => "300x300", :thumb => "100x100>" }
  validates_attachment_content_type :institution_image, :content_type => /\Aimage\/.*\Z/

end
