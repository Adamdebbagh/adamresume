class Article < ActiveRecord::Base

  is_impressionable
  acts_as_taggable_on :tags

  extend FriendlyId
  friendly_id :title, use: :slugged

  validates :title,:content,:tag_list, presence:true
  validates :title, length: {minimum: 5}
  validates :title, :uniqueness => {:message => ' has already been taken. please make a new one! '}

  has_attached_file :article_image, :styles => { :large => "900x500", :medium => "300x300", :thumb => "100x100>" }
  validates_attachment_content_type :article_image, :content_type => /\Aimage\/.*\Z/

end
