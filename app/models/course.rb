class Course < ActiveRecord::Base
  belongs_to :education
  validates :course_title,:course_grade,:establishment, presence:true


  has_attached_file :establishment_logo, :styles => {:thumb => "100x100>" }
  validates_attachment_content_type :establishment_logo, :content_type => /\Aimage\/.*\Z/

end
