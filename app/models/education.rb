class Education < ActiveRecord::Base
  has_many :courses
  validates :major,:university,:year, presence:true


  has_attached_file :university_logo, :styles => {:thumb => "100x100>" }
  validates_attachment_content_type :university_logo, :content_type => /\Aimage\/.*\Z/

end