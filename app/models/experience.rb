class Experience < ActiveRecord::Base
  validates :title,:organization,:period, presence:true
  validates :website, :format => URI::regexp(%w(http https))

end
