class LanguagesController < ApplicationController
  before_action :find_language, only: [:edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index]
  def index
    @languages = Language.all
    @candidate = User.first
  end

  def new
    @language = Language.new
  end

  def create
    @language = Language.new language_params
    if @language.save
      redirect_to languages_path, notice: 'New Language Skill Successfully Added'
    else
      render 'new', alert:"Oops, something went wrong.please try again"
    end
  end
  def edit
  end
  def update
    if @language.update language_params
      redirect_to languages_path, notice: 'Language Successfully Updated'
      else render 'edit', alert: 'Something went wrong, Try again'
    end
  end
  def destroy
    @language.destroy
    redirect_to languages_path
  end

  private
  def find_language
    @language = Language.find(params[:id])
  end
  def language_params
    params.require(:language).permit(:title, :level, :rate)
  end
end
