class ExperiencesController < ApplicationController
  before_action :find_experience, only: [:edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index]

  def index
    @experiences = Experience.all.order('created_at desc')
  end

  def new
    @experience = Experience.new
  end

  def create
    @experience = Experience.new experience_params
    if @experience.save
      redirect_to experiences_path, notice: 'Experience Successfully Saved'
    else
      render 'new', alert:'Oops Something went wrong. please try again'
    end
  end

  def edit
  end

  def update
    if @experience.update experience_params
      redirect_to experiences_path, notice: 'Experience Successfully Updated'
    else render 'edit', alert: 'Something went wrong, Try again'
    end
  end
  def destroy
    @experience.destroy
    redirect_to experiences_path
  end

  private
  def find_experience
    @experience = Experience.find(params[:id])
  end
  def experience_params
    params.require(:experience).permit(:title, :organization,:description, :website, :period)
  end


end
