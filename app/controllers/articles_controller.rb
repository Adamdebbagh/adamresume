class ArticlesController < ApplicationController
  before_action :find_article, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show, :tags]
  impressionist :actions=>[:show]

  def index
    @articles = Article.all.order('created_at desc').paginate(page: params[:page], per_page: 3)
    @candidate = User.first
    @recent_articles = Article.all.order('created_at ASC').limit(5) # Archived Articles
    @news_signup = Newsletter.new
  end

  def show
    impressionist(@article, unique: [:impressionable_type, :impressionable_id])
    @latest_articles = Article.all.order('created_at DESC').limit(3)
    @related_articles = Article.all.tagged_with(@article.tag_list, any: true).order('created_at DESC').limit(3)
    @info_image = Article.last
    @candidate = User.first
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new article_params
    if @article.save
      redirect_to @article, notice: 'Article successfully saved'
    else
      render 'new', alert:'Oops, something went wrong, please Try again'
    end
  end

  def edit
  end

  def update
    if @article.update article_params
      redirect_to @article, notice: 'Article successfully updated'
    else
      render 'edit', alert:'Oops, something went wrong, please Try again'
    end
  end

  def destroy
    @article.destroy
    redirect_to articles_path
  end

  def tags
    @candidate = User.first
    if params[:tag].present?
      @articles = Article.tagged_with(params[:tag]).order('created_at desc').paginate(page: params[:page], per_page: 3)
    else
      @articles = Article.all.order('created_at desc').paginate(page: params[:page], per_page: 3)
    end
    @related_articles = Article.tagged_with(params[:tag]).order('created_at asc').limit(3)
    @recent_articles = Article.all.order('created_at DESC').limit(5) # Latest Articles

  end

  private

  def find_article
    @article = Article.friendly.find(params[:id])
  end
  def article_params
    params.require(:article).permit(:title, :caption, :summary, :content,:markdown, :tag, :category, :article_image, :slug, :tag_list)
  end
end
