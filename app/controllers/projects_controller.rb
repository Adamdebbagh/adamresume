class ProjectsController < ApplicationController
  before_action :find_project, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]
  impressionist actions: [:show]


  def index
    @projects = Project.all.order('created_at desc')
    @candidate = User.first
    @skills = Skill.all.limit(4)
    @testimonials = Testimonial.all.limit(2)
    @educations = Education.all.limit(3)
    @conferences = Conference.all.limit(4)
    @languages = Language.all.limit(3)
  end

  def show
    @candidate = User.first
    @skills = Skill.all.limit(4)
    @testimonials = Testimonial.all.limit(2)
    @educations = Education.all.limit(3)
    @conferences = Conference.all.limit(4)
    @languages = Language.all.limit(3)
    impressionist(@project)
  end

  def new
    @project = Project.new
  end

  def create
    @project = Project.new project_params
    if @project.save
      redirect_to @project, notice: 'New Project Successfully Added'
    else
      render 'new', alert: 'Oops, something went wrong, Try again'
    end
  end

  def edit
  end

  def update
    if @project.update project_params
      redirect_to @project, notice: 'Project Successfully Updated'
    else render 'edit', alert: 'Something went wrong, Try again'
    end
  end
  def destroy
    @project.destroy
    redirect_to @project
  end

  private
  def find_project
    @project = Project.find(params[:id])
  end
  def project_params
    params.require(:project).permit(:title, :subtitle,:description, :link, :project_image, :github_link, :featured)
  end


end
