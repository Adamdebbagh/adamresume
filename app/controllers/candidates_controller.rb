class CandidatesController < ApplicationController
  def index
    @candidates = User.all
    @candidate = @candidates.first
    unless  @candidate.github.blank?
      @username = URI(@candidate.github).path.split('/').last
    end
    @skills = Skill.all.limit(7)
    @testimonials = Testimonial.all.limit(2)
    @educations = Education.all.limit(3)
    @certificates = Certificate.all.limit(3)
    @institutions = Institution.all.limit(5).order('created_at desc')
    @conferences = Conference.all.limit(4)
    @languages = Language.all.order('created_at desc')
    @projects = Project.all.order('created_at desc').limit(3)
    @featured_project =  Project.where(featured: true ).first
    @experiences = Experience.all.order('created_at desc').limit(3)
    @recent_articles = Article.all.order('created_at DESC').limit(2)
    @contact = Contact.new

  end


end
