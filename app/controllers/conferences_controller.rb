class ConferencesController < ApplicationController
  before_action :conference_params, only: [:create, :update]
  before_action :find_conference, only: [:edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index]
  def index
    @conferences = Conference.all
    @candidate = User.first
  end

  def new
    @conference = Conference.new
  end

  def create
    @conference = Conference.new conference_params
    if @conference.save
      redirect_to conferences_path, notice: 'Your Conference was successfully Saved'
    else
      render 'new', alert: 'Your submission failed, Please Try again'
    end
  end

  def edit
  end

  def update
    if @conference.update(conference_params)
      redirect_to conferences_path , notice: 'Conference successfully updated'
    else
      render 'edit', alert: 'Oops, please try again'
    end
  end

  def destroy
    @conference.destroy
    redirect_to conferences_path
  end

  private
  def find_conference
    @conference = Conference.find(params[:id])
  end
  def conference_params
    params.require(:conference).permit(:name, :location, :link)
  end

end
