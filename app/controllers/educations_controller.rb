class EducationsController < ApplicationController
  before_action :find_education, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]
  def index
    @educations = Education.all
    @candidate = User.first
  end

  def new
    @education = Education.new
  end
  def create
    @education = Education.new education_params
    if @education.save
      redirect_to educations_path, notice:'Successfully Saved!'
    else
      render 'new', alert:'Failed to save, please Try again'
    end
  end

  def show
    @education = Education.find(params[:id])

    @courses = Course.where(education_id: @education)
  end

  def edit
    #@course = Course.find(params[:id])
  end
  def update
    if @education.update_attributes(education_params)
      redirect_to educations_path, notice: 'Successfully Updated'
    else
      render 'edit', alert: 'Update failed, please try again'
    end
  end

  def destroy
    @education.destroy
    redirect_to @education
  end


  private
  def find_education
    @education = Education.find(params[:id])
  end
  def education_params
    params.require(:education).permit(:major, :university, :year,:university_logo)
  end
end
