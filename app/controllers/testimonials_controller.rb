class TestimonialsController < ApplicationController
  before_action :testimonial_params, only: [:create, :update]
  before_action :find_testimonial, only: [:edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index]
  def index
    @testimonials = Testimonial.all
    @candidate = User.first
  end

  def new
    @testimonial = Testimonial.new
  end

  def create
    @testimonial = Testimonial.new testimonial_params
    if @testimonial.save
      redirect_to testimonials_path, notice: 'Your Testimonial was successfully Saved'
    else
      render 'new', alert: 'Oops, something went wrong, Try again'
    end
  end

  def edit
  end

  def update
    if @testimonial.update(testimonial_params)
      redirect_to testimonials_path , notice: 'Testimonial successfully updated'
    else
      render 'edit', alert: 'Oops, something went wrong, Try again'
    end
  end

  def destroy
    @testimonial.destroy
    redirect_to testimonials_path
  end

  private
  def find_testimonial
    @testimonial = Testimonial.find(params[:id])
  end
  def testimonial_params
    params.require(:testimonial).permit(:name, :company, :quote)
  end
end
