class NewslettersController < ApplicationController

  def new
    @news_signup = Newsletter.new
  end
  def create
    @news_signup = Newsletter.new(newsletter_params)
    if @news_signup.save
      redirect_to articles_path
    else
      redirect_to articles_path, alert:'Oops, something went wrong, please Try again'
    end
  end

  private

  def newsletter_params
    params.require(:newsletter).permit(:email)
  end

end
