class SkillsController < ApplicationController
  before_action :find_skill, only: [:edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index]

  def index
    @skills = Skill.all.order('created_at desc')
    @candidate = User.first
  end
  def new
   @skill = Skill.new
  end

  def create
    @skill = Skill.new skill_params
    if @skill.save
      redirect_to skills_path , notice: 'Your Skill has been Successfully Added'
    else
      render 'new', alert: 'Oops, something went wrong, Try again'
    end
  end

  def edit
  end
  def update
    @skill = Skill.find(params[:id])
    if @skill.update(skill_params)
      redirect_to skills_path , notice: 'Skill successfully updated'
    else
      render 'edit', alert: 'Oops, something went wrong, Try again'
    end

  end
  def destroy
    @skill.destroy
    redirect_to skills_path
  end
  private
  def find_skill
    @skill = Skill.find(params[:id])
  end
  def skill_params
    params.require(:skill).permit(:intro, :skill_title, :skill_level, :skill_in_percentage)
  end
end
