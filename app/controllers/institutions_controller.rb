class InstitutionsController < ApplicationController
  before_action :find_certificate
  before_action :find_institution, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]

  def index
    @institutions = Institution.all.order('created_at desc')
    @candidate = User.first
  end
  def show
    @certificates = Certificate.where(institution_id: @institution).order("created_at DESC")

  end
  def new
    @institution = Institution.new
  end

  def create
    @institution = Institution.new institution_params
    if @institution.save
      redirect_to institutions_path, notice: 'Institution successfully saved'
    else
      render 'new', alert:'Oops, something went wrong, please Try again'
    end
  end

  def edit
  end

  def update
    if @institution.update institution_params
      redirect_to institutions_path, notice: 'Institution successfully updated'
    else
      render 'edit', alert: 'Oops, something went wrong, please Try again'
    end
  end

  def destroy
    @institution.destroy
    redirect_to institutions_path
  end

  private

  def find_certificate
    @certificates = Certificate.where(institution_id: @institution).order("created_at DESC").paginate(:page => params[:page], :per_page => 5)
  end

  def find_institution
    @institution = Institution.find(params[:id])
  end

  def institution_params
    params.require(:institution).permit(:title, :website, :description, :institution_image)
  end

end
