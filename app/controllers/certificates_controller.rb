class CertificatesController < ApplicationController
  before_action :find_institution
  before_action :find_certificate, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]


  def index
    @certificates = Certificate.all.order('created_at desc')
    @candidate = User.first
    @institution = Institution.find(params[:institution_id])
  end

  def show
    @certificates = Certificate.where(institution_id: @institution).order("created_at DESC")
  end

  def new
    @institution = Institution.find(params[:institution_id])
    @certificate =  @institution.certificates.new
  end
  def create
    @certificate = @institution.certificates.new certificate_params
    if @certificate.save
      redirect_to institutions_path, notice: 'Certificate successfully saved'
    else
      render 'new', alert:'Oops, something went wrong, please Try again'
    end
  end
  def edit
  end
  def update
    if @certificate.update certificate_params
      redirect_to institutions_path, notice: 'Certificate successfully updated'
    else
      render 'edit', alert:'Oops, something went wrong, please Try again'
    end
  end

  def destroy
    @certificate.destroy
    redirect_to institutions_path
  end

  private

  def find_institution
    @institution = Institution.find(params[:institution_id])
  end

  def find_certificate
    @certificate = Certificate.find(params[:id])
  end
  def certificate_params
    params.require(:certificate).permit(:institution_title, :course_title, :score, :course_issuer, :course_instructor, :description, :institution_logo, :course_link)
  end
end
