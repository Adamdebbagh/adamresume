class CoursesController < ApplicationController
  before_action :find_education
  before_action :find_course, only: [:edit, :update, :destroy]
  before_action :authenticate_user!
  def new
    @education = Education.find(params[:education_id])
    @course = @education.courses.new
  end
  def create
    @course = @education.courses.new(course_params)
    if @course.save
      redirect_to educations_path, notice:'Successfully Saved!'
    else
      render 'new', alert:'Failed to save, please Try again'
    end
  end
  def edit
    @education = Education.find(params[:education_id])
  end
  def update
    if @course.update_attributes(course_params)
      redirect_to educations_path, notice: 'Successfully Updated'
    else
      render 'edit', alert: 'Update failed, please try again'
    end
  end

  def destroy
    @course.destroy
    redirect_to educations_path
  end

  private

  def find_course
    @course = Course.find(params[:id])
  end
  def find_education
    @education = Education.find(params[:education_id])
  end
  def course_params
    params.require(:course).permit(:course_title, :course_grade, :establishment, :instructor, :description, :establishment_logo)
  end
end
